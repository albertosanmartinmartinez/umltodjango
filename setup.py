
from setuptools import setup

setup(
    name='umltodjango',
    version='1.0.0',
    author='Alberto Sanmartin Martinez',
    author_email='info@albertosanmartinmartinez.es',
    packages=['umltodjango'],
    url='https://gitlab.com/albertosanmartinmartinez/umltodjango',
    license='LICENSE.txt',
    description='An awesome package to conver UML Diagrams to Django Models',
    long_description=open('README.md').read(),
    install_requires=[]
)