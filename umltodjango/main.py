
import pprint
import xml.etree.ElementTree as et

from contextlib import redirect_stdout


class Node:
    """
    """

    def __init__(self, value=None):

        if not value:
            raise Exception("Node should have value.")
        
        self.value = value
        self.children = []
    
    def __repr__(self):

        return str(self.value)
    
    def add_child(self, child):

        self.children.append(child)


class Graph:
    """
    """

    def __init__(self, root=None):
        
        if not root:
            raise Exception("Graph should have root node.")
        
        if not isinstance(root,Node):
            raise Exception("Graph root should be Node instance.")

        self.root = root

    def bfs(self):

        self.visited = []
        self.queue = []
        self.result = []

        self.visited.append(self.root)
        self.queue.append(self.root)

        while self.queue:
            
            tmp = self.queue.pop(0)
            self.result.append(tmp)
            
            for child in tmp.children:
                if child not in self.visited:
                    self.visited.append(child)
                    self.queue.append(child)
        
        return self.result


class UMLTODJANGO:
    """
    """

    models = {}
    relations = {}

    root = Node('root')
    graph = Graph(root)

    def __create_model(self, model_id):

        self.models[model_id] = {}
        self.models[model_id]['attrs'] = []
        self.models[model_id]['parents'] = []
        self.models[model_id]['relations'] = {}
        self.models[model_id]['children'] = []

    def build_deps(self):
        """
        """

        for key, model in self.models.items():
            
            model_node = Node(key)

            if not model['parents']:
                self.root.add_child(model_node)
            
            if model['children']:
                for child in model['children']:
                    child_node = Node(child)
                    model_node.add_child(child_node)

    def export(self, file=None):
        """
        """

        if not file:
            file = 'exported.json'

        with open(file, 'w') as f:
            with redirect_stdout(f):
                pprint.pprint(self.models)
                
    def __write_model(self, model):
        
        model_lines = []
        
        model_lines.append('')
        
        text = f'class { model["label"] }('

        if model['parents']:
            parent_ids = list(map(lambda x: self.models[x]['label'], model['parents']))
            parent_label = ', '.join(parent_ids)
            text += parent_label
        
        else:
            text += 'models.Model'
        
        text += '):'

        model_lines.append(text)
        model_lines.append('')

        return model_lines

    def __write_attribute(self, attribute):
        
        attribute_lines = []

        name = attribute['label'].split(':')[0].strip()
        type = attribute['label'].split(':')[1].strip()
        text = f"    { name } = models.{ type }"

        attribute_lines.append(text)

        return attribute_lines

    def __write_relation(self, relation):
        
        relation_lines = []

        text = f"    { relation['label'] } = models."

        if relation['multiplicity'] == '1..1':
            text += 'OneToOne'
        
        elif relation['multiplicity'] == ("0..*") or relation['multiplicity'] == ("1..*"):
            text += 'ForeignKey'
        
        elif relation['multiplicity'] == "*..*":
            text += 'ManyToMany'

        text += f"(\'{ relation['target'] }\')"
        
        relation_lines.append(text)

        return relation_lines

    def write(self):
        """
        1..1 - Django OneToOne
        0..* - Django ForeignKey with null=True
        1..* - Django ForeignKey 
        *..* - Django ManyToMany
        """

        file_lines = []

        file_lines.append('from django.db import models')
        file_lines.append('')
        
        for key in self.graph.bfs()[1:]:
            
            model = self.models[key.value]

            file_lines = file_lines + self.__write_model(model)

            if model['attrs']:
                for attribute in model['attrs']:
                    file_lines = file_lines + self.__write_attribute(attribute)
            
            if model['relations']:
                file_lines.append('')
                for relation_id, relation in model['relations'].items():
                    file_lines = file_lines + self.__write_relation(relation)  
                
            if not model['attrs'] and not model['relations']:
                
                file_lines.append('    pass')
            
            file_lines.append('')

            file_lines.append('    class Meta:')
            file_lines.append('        abstract = True')

            file_lines.append('')
        
        try:
            file = open('result.py', 'w')

            for line in file_lines:
                file.write(f'{ line }\n')

            file.close()
        
        except Exception as e:
            print(e)



    def parse(self, file=None):
        """
        """
        
        if not file:
            file = 'example.xml'
        
        tree = et.parse(file)
        iter = tree.iter()

        for node in iter:

            attributes = node.attrib
            
            if 'type' in attributes:

                if attributes['type'] == 'model':
                    
                    model_id = attributes.pop('id', None)
                    label = attributes.pop('label', None)

                    if not model_id:
                        raise Exception(f'Not set id attribute on XML node { node }.')
                    
                    if not label:
                        raise Exception(f'Not set label attribute on XML node { node }.')

                    if model_id not in self.models:

                        self.__create_model(model_id)
                    
                    self.models[model_id]['label'] = label

                elif attributes['type'] == 'attribute':
                    
                    child = node.find('mxCell')

                    if not child:
                        raise Exception(f'Not set mxCell child on XML node { node }.')
                        
                    parent_id = child.attrib.pop('parent', None)

                    if not parent_id:
                        raise Exception(f'Not set parent attribute on XML node { node }.')

                    if parent_id not in self.models:

                        self.__create_model(parent_id)

                    self.models[parent_id]['attrs'].append(attributes)
                
                elif attributes['type'] == 'inheritance':

                    child = node.find('mxCell')

                    if not child:
                        raise Exception(f'Not set mxCell child on XML node { node }.')
                        
                    source_id = child.attrib.pop('source', None)

                    if not source_id:
                        raise Exception(f'Not set source attribute on XML node { node }.')

                    if source_id not in self.models:
                        
                        self.__create_model(source_id)

                    target_id = child.attrib.pop('target', None)

                    if not target_id:
                        raise Exception(f'Not set target attribute on XML node { node }.')

                    if target_id not in self.models:
                        
                        self.__create_model(target_id)

                    self.models[source_id]['parents'].append(target_id)
                    self.models[target_id]['children'].append(source_id)
                
                elif attributes['type'] == 'relation':

                    child = node.find('mxCell')
                    
                    if not child:
                        raise Exception(f'Not set mxCell child on XML node { node }.')

                    source_id = child.attrib.pop('source', None)

                    if not source_id:
                        raise Exception(f'Not set source attribute on XML node { node }.')

                    if source_id not in self.models:
                        
                        self.__create_model(source_id)

                    target_id = child.attrib.pop('target', None)

                    if not target_id:
                        raise Exception(f'Not set target attribute on XML node { node }.')

                    if target_id not in self.models:
                        
                        self.__create_model(target_id)
                    
                    relation_id = attributes.get('id', None)

                    if not relation_id:
                        raise Exception(f'Not set id attribute on XML node { node }.')
                        
                    attributes['target'] = self.models[target_id]['label']
                    attributes['multiplicity'] = ''

                    self.models[source_id]['relations'][relation_id] = attributes

                    self.relations[relation_id] = source_id
                
                elif attributes['type'] == 'multiplicity':
                    
                    child = node.find('mxCell')

                    if not child:
                        raise Exception(f'Not set mxCell child on XML node { node }.')

                    relation_id = child.attrib.pop('parent', None)

                    if not relation_id:
                        raise Exception(f'Not set parent attribute on XML node { child }.')
                        
                    label = node.attrib.pop('label', None)

                    if not label:
                        raise Exception(f'Not set label attribute on XML node { node }.')

                    parent_id = self.relations[relation_id]

                    self.models[parent_id]['relations'][relation_id]['multiplicity'] = label