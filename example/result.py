from django.db import models


class Vehicle(models.Model):

    model = models.CharField(max_length=256)
    brand = models.CharField(max_length=256)

    class Meta:
        abstract = True


class Person(models.Model):

    name = models.CharField(max_length=256)

    class Meta:
        abstract = True


class Category(models.Model):

    name = models.CharField(max_length=256)

    class Meta:
        abstract = True


class Car(Vehicle):

    plate = models.CharField(max_length=256)

    category = models.ForeignKey('Category')
    owner = models.ForeignKey('Person')

    class Meta:
        abstract = True


class ElectricCar(Car, Person):

    charger = models.IntegerField

    class Meta:
        abstract = True

