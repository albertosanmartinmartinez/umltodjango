
from umltodjango.main import UMLTODJANGO

def example():
    
    umltodjango = UMLTODJANGO()
    umltodjango.parse()
    umltodjango.export()
    umltodjango.build_deps()
    umltodjango.write()


if __name__ == '__main__':
    
    example()